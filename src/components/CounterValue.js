import React, { Component } from 'react'
import './Counter.css';

export default class CounterValue extends Component {
    render() {
        return (
            <div className="counterValue">
                Counter Value: {this.props.counterValue}
            </div>
        )
    }
}
