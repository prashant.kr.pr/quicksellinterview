import React, { Component } from 'react'
import axios from 'axios';
import { COUNTER_MAX_VALUE, COUNTER_MIN_VALUE } from '../utils/constant';
import './Counter.css';
import CounterValue from './CounterValue';

const INCREMENT = 'increment';
const DECREMENT = 'decrement';
const INPUT_FIELD = 'input_field';
const REQUESTING = 'requesting';
const SUCCESS = 'success';
const LOADER_TEXT = 'Saving counter value';
const BASE_URL = 'https://interview-8e4c5-default-rtdb.firebaseio.com/';

export default class counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counterValue: COUNTER_MIN_VALUE,
            maxCounterValue: COUNTER_MAX_VALUE,
            apiStatus: REQUESTING
        }
    }

    componentDidMount() {
        const apiUrl = BASE_URL + 'front-end/counter1.json';
        var self = this;
        axios.get(apiUrl)
            .then(function (response) {
                let counterData = response && response.data && response.data.counter1;
                if (counterData) {
                    self.setState({ counterValue: counterData, apiStatus: SUCCESS });
                } else {
                    self.setState({ counterValue: COUNTER_MIN_VALUE, apiStatus: SUCCESS });
                }
            })
            .catch(function (error) {
                this.setState({ counterValue: COUNTER_MIN_VALUE, apiStatus: 'error' });
            });
    }

    updateCounter(value) {
        const apiUrl = BASE_URL + 'front-end/counter1.json';
        this.setState({ apiStatus: REQUESTING });
        var self = this;
        axios({
            method: 'put',
            url: apiUrl,
            data: {
                counter1: value,
            }
        }).then(function (response) {
            let counterData = response && response.data && response.data.counter1;
            if (counterData) {
                self.setState({ counterValue: counterData, apiStatus: SUCCESS });
            } else {
                self.setState({ counterValue: COUNTER_MIN_VALUE, apiStatus: SUCCESS });
            }
        }).catch(function (error) {
            this.setState({ counterValue: COUNTER_MIN_VALUE, apiStatus: 'error' });
        });
    }

    counterChange(type, value = this.state.counterValue) {
        if (type === INCREMENT &&
            this.state.counterValue < COUNTER_MAX_VALUE &&
            this.state.counterValue >= COUNTER_MIN_VALUE) {
            let updatedValue = this.state.counterValue + 1;
            if (this.state.apiStatus !== REQUESTING) {
                this.updateCounter(updatedValue);
            }

        } else if (type === DECREMENT &&
            this.state.counterValue <= COUNTER_MAX_VALUE &&
            this.state.counterValue > COUNTER_MIN_VALUE) {
            let updatedValue = this.state.counterValue - 1;
            if (this.state.apiStatus !== REQUESTING) {
                this.updateCounter(updatedValue);
            }
        } else if (type === INPUT_FIELD &&
            value <= COUNTER_MAX_VALUE &&
            value >= COUNTER_MIN_VALUE) {
            let updatedValue = parseInt(value);
            if (this.state.apiStatus !== REQUESTING) {
                this.updateCounter(updatedValue);
            }
        }
    }
    render() {
        return (
            <div className="parentDiv">
                {
                    this.state.apiStatus === REQUESTING ? (
                        <div className="loaderDiv">
                            <div class="sk-circle">
                                <div class="sk-circle1 sk-child"></div>
                                <div class="sk-circle2 sk-child"></div>
                                <div class="sk-circle3 sk-child"></div>
                                <div class="sk-circle4 sk-child"></div>
                                <div class="sk-circle5 sk-child"></div>
                                <div class="sk-circle6 sk-child"></div>
                                <div class="sk-circle7 sk-child"></div>
                                <div class="sk-circle8 sk-child"></div>
                                <div class="sk-circle9 sk-child"></div>
                                <div class="sk-circle10 sk-child"></div>
                                <div class="sk-circle11 sk-child"></div>
                                <div class="sk-circle12 sk-child"></div>
                            </div>
                            <div>{LOADER_TEXT}</div></div>
                    ) : null
                }
                <div className="mainDiv">
                    <div className="decrementCounter" onClick={() => this.counterChange(DECREMENT)}>-</div>
                    <input
                        value={this.state.counterValue}
                        type="number"
                        className="inputField"
                        onChange={(e) => this.counterChange(INPUT_FIELD, e.target.value)}
                    />
                    <div className="incrementCounter" onClick={() => this.counterChange(INCREMENT)}>+</div>

                </div>
                <CounterValue
                    counterValue={this.state.counterValue}
                />
            </div>
        )
    }
}
